package ManUtd;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class DB {




   /*final String JDBC_DRIVER ="com.mysql.cj.jdbc.Driver";
    final String URL = "jdbc:mysql://localhost:3306/telefonszámok?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
   */

    final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    final String URL = "jdbc:mysql://localhost:3306/manchesterutd?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    final String USERNAME ="root";
    final String PASSWORD ="";

    //Létrehozzuk a kapcsolatot (hidat)
    Connection conn = null;
    Statement createStatement = null;
    DatabaseMetaData dbmd = null;


    public DB() {
        //Megpróbáljuk életre kelteni
        try {
            conn = DriverManager.getConnection(URL,USERNAME,PASSWORD);
            System.out.println("A híd létrejött");
        } catch (SQLException ex) {
            System.out.println("Valami baj van a connection (híd) létrehozásakor.");
            System.out.println("" + ex);
        }

        //Ha életre kelt, csinálunk egy megpakolható teherautót
        if (conn != null) {
            try {
                createStatement = conn.createStatement();
            } catch (SQLException ex) {
                System.out.println("Valami baj van van a createStatament (teherautó) létrehozásakor.");
                System.out.println("" + ex);
            }
        }

        //Megnézzük, hogy üres-e az adatbázis? Megnézzük, létezik-e az adott adattábla.
        try {
            dbmd = conn.getMetaData();
        } catch (SQLException ex) {
            System.out.println("Valami baj van a DatabaseMetaData (adatbázis leírása) létrehozásakor..");
            System.out.println("" + ex);
        }

        try {
            ResultSet rs = dbmd.getTables(null, "APP", "USERS", null);
            if (!rs.next()) {
                //createStatement.execute("create table manchester(Ellenfél varchar(20), Idegenben varchar(20), Dátum varchar(20), Rugott_gól varchar(20), Kapott_gól varchar(20))");
                //createStatement.execute("drop table manchester");
            }
        } catch (SQLException ex) {
            System.out.println("Valami baj van az adattáblák létrehozásakor.");
            System.out.println("" + ex);
        }
    }

    /** Mérkőzések hozzáadása az adatbázishoz*/
  public void addUser(String Ellenfél, String Idegenben, String Tipp, String Eredmény, String Dátum, int Rugott_gól, int Kapott_gól ) {
        try {
//            String sql = "insert into users values ('" + Ellenfél + "','" + Idegenben + "','" + Dátum + "','" + Rugott_gól + "','" + Kapott_gól + "')";
//            createStatement.execute(sql);
            String sql = "insert into manchesterutd values (?,?,?,?,?,?,?)";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, Ellenfél);
            preparedStatement.setString(2, Idegenben);
            preparedStatement.setString(3,Tipp);
            preparedStatement.setString(4,Eredmény);
            preparedStatement.setString(5, Dátum);
            preparedStatement.setInt(6, Rugott_gól);
            preparedStatement.setInt(7, Kapott_gól);


            preparedStatement.execute();
        } catch (SQLException ex) {
            System.out.println("Valami baj van a user hozzáadásakor");
            System.out.println("" + ex);
        }
    }

        /* Kiírja konzolra az összes mérkőzést*/
    public void showAllUsers() {
        String all = "";
        String sql = "select * from manchesterutd";
        try {
            ResultSet rs = createStatement.executeQuery(sql);
            while (rs.next()) {
                String Ellenfél = rs.getString("Ellenfél");
                String Idegenben = rs.getString("Idegenben");
                String Tipp = rs.getString("Tipp");
                String Eredmény = rs.getString("Eredmény");
                String Dátum = rs.getString("Dátum");
                int Rugott_gól = rs.getInt("RugottGól");
                int Kapott_gól = rs.getInt("KapottGól");
                System.out.println(Ellenfél + " | " + Idegenben+" | "+ " | " + Tipp + " | " + " | " +Eredmény + " | " + Dátum + " | " + Rugott_gól+" | "+Kapott_gól);

            }
        } catch (SQLException ex) {
            System.out.println("Valami baj van a userek kiolvasásakor");
            System.out.println("" + ex);
        }
    }

    public void showUsersMeta(){
        String sql = "select * from manchesterutd";
        ResultSet rs = null;
        ResultSetMetaData rsmd = null;
        try {
            rs = createStatement.executeQuery(sql);
            rsmd = rs.getMetaData();
            int columnCount = rsmd.getColumnCount();
            for (int x = 1; x <= columnCount; x++){
                System.out.print(rsmd.getColumnName(x) + " | ");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /** Az összes mérkőzés ArrayList-be gyűjtése*/
    public ArrayList<User> getAllUsers(){
        String sql = "select * from manchesterutd";
        ArrayList<User> users = null;
        try {
            ResultSet rs = createStatement.executeQuery(sql);
            users = new ArrayList<>();

            while (rs.next()){
                User actualUser = new User(rs.getString("Ellenfél"),rs.getString("Idegenben"),rs.getString("Tipp"), rs.getString("Eredmény"), rs.getString("Dátum"),rs.getInt("RugottGól"),rs.getInt("KapottGól"));
                users.add(actualUser);
            }
        } catch (SQLException ex) {
            System.out.println("Valami baj van a userek kiolvasásakor");
            System.out.println(""+ex);
        }

        return users;
    }

  /*  public void addUser(User manu){

        try {
            String sql = "insert into machesterutd values(?,?,?,?,?)";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, manu.setEllenfél());
            preparedStatement.setString(2, manu.setIdegenben());
            preparedStatement.setString(1, manu.setTipp());
            preparedStatement.setString(2, manu.setEredmény());
            preparedStatement.setString(3, manu.setDátum());
            preparedStatement.setInt(4, manu.setRugott_gól());
            preparedStatement.setInt(5, manu.setKapott_gól());
            preparedStatement.execute();
        } catch (SQLException ex) {
            System.out.println("Valami baj van a user hozzáadásakor");
            System.out.println(""+ex);
        }
    }*/

}