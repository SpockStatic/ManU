package ManUtd;

import javax.swing.*;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.Scanner;


public class C2 extends JFrame {

    private JLabel j1;
    private JLabel j2;
    private JLabel j3;
    private JLabel j4;
    private JLabel j5;
    private JLabel j6;
    private JLabel j7;


    private TextField ellenfel;
    private TextField home;
    private TextField tipp;
    private TextField eredmeny;
    private TextField date;
    private TextField rugott;
    private TextField kapott;
    
    private JButton b1;
    private JButton b2;
    private JButton b3;
    private JButton b4;
    private JButton b5;
    private JButton b6;


    C2() {
        super("Mérkőzések");
        setLayout(null);

        j1 = new JLabel("Ellenfél: ");
        j2 = new JLabel("Idegenben: ");
        j3 = new JLabel("Tipp: ");
        j4 = new JLabel("Eredmény: ");
        j5 = new JLabel("Dátum: ");
        j6 = new JLabel("Rugott: ");
        j7 = new JLabel("Kapott: ");



        ellenfel = new TextField(10);
        home = new TextField(10);
        tipp = new TextField(10);
        eredmeny=new TextField(10);
        date = new TextField("0000-00-00");
        rugott = new TextField(5);
        kapott = new TextField(5);

        j1.setBounds(10,10,50,25);
        add(j1);
        ellenfel.setBounds(60,10,125,25);
        add(ellenfel);
        j2.setBounds(220,10,80,25);
        add(j2);
        home.setBounds(300,10,125,25);
        add(home);
        j3.setBounds(10,50,50,25);
        add(j3);
        tipp.setBounds(60,50,125,25);
        add(tipp);
        j4.setBounds(220,50,80,25);
        add(j4);
        eredmeny.setBounds(300,50,125,25);
        add(eredmeny);
        j5.setBounds(10,130,50,25);
        add(j5);
        date.setBounds(60,130,125,25);
        add(date);
        j6.setBounds(10,90,50,25);
        add(j6);
        rugott.setBounds(60,90,125,25);
        add(rugott);
        j7.setBounds(220,90,80,25);
        add(j7);
        kapott.setBounds(300,90,125,25);
        add(kapott);

        b1 = new JButton("Hozzáadás");
        b1.setBounds(10,190,150,30);
        add(b1);

        b4 = new JButton("Összes Mérkőzés");
        b4.setBounds(165,190,150,30);
        add(b4);

        b6 = new JButton("Minden Mérkőzés");
        b6.setBounds(320,190,150,30);
        add(b6);


        b6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                C5 c5 = new C5();
                c5.setVisible(true);
                c5.setBounds(300,300,300,500);

            }
        });

        b4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                C4 c4 = new C4();
                c4.setVisible(true);
                c4.setBounds(300,300,425,350);
                c4.setResizable(false);


            }
        });

        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String ell = ellenfel.getText();
                String ide = home.getText();
                String ti = tipp.getText();
                String er = eredmeny.getText();
                String da = date.getText();
                String rugString = rugott.getText();
                String kapString = kapott.getText();

                int rug =Integer.parseInt(rugString);
                int kap = Integer.parseInt(kapString);

                DB db = new DB();
                db.addUser(ell,ide,ti,er,da,rug,kap);

            }
        });


    }
}
